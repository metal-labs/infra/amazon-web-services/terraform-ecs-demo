// Import the express library
const express = require('express');

// Initialize the express application
const app = express();

// Define a route for the root URL
app.get('/', (req, res) => {
  res.send('Hello, World! this is version 1.1.1');
});

// Start the server and listen on port 3000
app.listen(3000, () => {
  console.log('Server running on http://localhost:3000/');
});
