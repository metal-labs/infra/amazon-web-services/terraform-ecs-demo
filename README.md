# Terraform ECS Demo

## about

This demo will create an AWS ECS Cluster with a node.js app behind a load balancer. After running `terraform apply` you will get an output with the load balancer url.

## Getting Started

- Set up AWS IAM Developer user account
- run `aws configure` and set the AWS Access Key and Secret Access keys with aws-cli on machine
- ensure the version set in the deploy.sh file is the same version in the `ecs.tf` image parameter
- run `cd with-terraform`
- run `terraform validate` to validate the IaC
- run `terraform plan` to view the plan
- run `terraform apply` to deploy the infra stack to AWS
- run `./scripts/deploy.sh` to build the docker container and push it to AWS ECR (Elastic Container Registry) Note: until this deploy is pushed up to the ERC, you'll get a 503 on the load balancer url. It will take a moment for this deploy script to push the containers to the service container and for it to realize the new images are there and for them to be live ;).
