#!/bin/bash

REPO=app-repo
TAG=v1.1.1
ACCOUNT_NUMBER=$(aws sts get-caller-identity --query Account --output text)

aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin $ACCOUNT_NUMBER.dkr.ecr.us-east-1.amazonaws.com

docker build -t $REPO:$TAG ../app

docker tag $REPO:$TAG $ACCOUNT_NUMBER.dkr.ecr.us-east-1.amazonaws.com/$REPO:$TAG

docker push $ACCOUNT_NUMBER.dkr.ecr.us-east-1.amazonaws.com/$REPO:$TAG
