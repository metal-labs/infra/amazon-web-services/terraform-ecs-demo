resource "aws_ecr_repository" "app_ecr_repo" {
  name = "app-repo"
  force_delete = false  # Forcefully delete repository if not empty
}
